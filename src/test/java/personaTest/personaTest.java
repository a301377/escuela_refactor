package personaTest;

import connections.PostgreSQL;
import factories.enums.Genero;
import factories.interfaces.PersonaDao;
import models.Persona;
import org.junit.Assert;
import org.junit.Test;

import java.sql.*;
import java.util.List;
import java.util.Scanner;

public class personaTest {

    @Test
    public void createTest(){
        PersonaDao personaDaoInterface = new factories.implementations.postgresql.PersonaDao();
        Persona persona = new Persona(new Long("1"), "Andree", "Calle 1", new Long("6143447198"), "", "", Genero.NO_DISPONIBLE);
        personaDaoInterface.create(persona);
    }

    @Test
    public void readTest(){
        PersonaDao personaDaoInterface = new factories.implementations.postgresql.PersonaDao();
        List<Persona> personaList = personaDaoInterface.read("");
        Assert.assertNotNull(personaList);
        for (Persona persona : personaList){
            System.out.println("persona = " + persona.getNombre());
            Assert.assertNotNull(persona);
        }
    }

    @Test
    public void readByIdTest(){
        PersonaDao personaDaoInterface = new factories.implementations.postgresql.PersonaDao();
        Persona persona = personaDaoInterface.read(new Long("2"));
        Assert.assertNotNull(persona);
        System.out.println(persona.getNombre());
    }

    @Test
    public void updateTest(){
        PersonaDao personaDaoInterface = new factories.implementations.postgresql.PersonaDao();
        Persona persona = personaDaoInterface.read(new Long("2"));
        persona.setNombre("Gisela");
        persona.setDomicilio("Campo Bello");
        persona.setTelefono(new Long("6143950178"));
        persona.setEmail("giselita@gmail.com");
        persona.setFechaNacimiento("hoy");
        persona.setGenero(Genero.FEMENINO);
        personaDaoInterface.update(persona);
    }

    @Test
    public void deleteTest(){
        PersonaDao personaDaoInterface = new factories.implementations.postgresql.PersonaDao();
        personaDaoInterface.delete(new Long("123"));
    }

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        Long id;
        String nombre;
        String domicilio;
        Long telefono;
        String email;
        String fechaNacimiento;
        Genero genero = Genero.NO_DISPONIBLE;
        String generoDescripcion;

        PersonaDao personaDaoInterface = new factories.implementations.postgresql.PersonaDao();
        System.out.println("Inserte un id");
        id = reader.nextLong();
        System.out.println("Inserte un nombre");
        nombre = reader.next();
        System.out.println("Inserte un domicilio");
        domicilio = reader.next();
        System.out.println("Inserte un telefono");
        telefono = reader.nextLong();
        System.out.println("Inserte un email");
        email = reader.next();
        System.out.println("Inserte una fecha de nacimiento");
        fechaNacimiento = reader.next();
        System.out.println("Inserte un genero (Masculino o Femenino");
        generoDescripcion = reader.next();

        if (generoDescripcion.equals("Masculino")){
            genero = Genero.MASCULINO;
        } else if (genero.equals("Femenino")){
            genero = Genero.FEMENINO;
        } else {
            genero = Genero.NO_DISPONIBLE;
        }
        Persona persona = new Persona(id, nombre, domicilio, telefono, email, fechaNacimiento, genero);
        personaDaoInterface.create(persona);
    }
}
