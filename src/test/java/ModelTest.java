import models.Model;
import org.junit.Assert;
import org.junit.Test;

public class ModelTest {
    @Test
    public void fieldsToInsertTest(){
        String okValue = "( ?, ?)";
        Assert.assertEquals(okValue, Model.fieldsToInsert(2));
    }
}

