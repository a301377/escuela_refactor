package factories.implementations.postgresql;

import connections.PostgreSQL;
import factories.enums.Genero;
import models.Persona;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class PersonaDao implements factories.interfaces.PersonaDao {

    @Override
    public void create(Persona obj) {
        try {
            PostgreSQL conexion = PostgreSQL.getInstance();
            PreparedStatement ps = conexion.getConn().prepareStatement(Persona.INSERT);
            Integer i = 1;
            ps.setLong(i++, obj.getId());
            ps.setString(i++, obj.getNombre());
            ps.setString(i++, obj.getDomicilio());
            ps.setLong(i++, obj.getTelefono());
            ps.setString(i++, obj.getEmail());
            ps.setString(i++, obj.getFechaNacimiento());
            ps.setString(i++, obj.getGenero().getDescripcion());
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("e = " + e.toString());
            System.out.println("obj = " + obj.getId());
            System.out.println("obj = " + obj.getNombre());
            System.out.println("obj = " + obj.getDomicilio());
            System.out.println("obj = " + obj.getTelefono());
            System.out.println("obj = " + obj.getEmail());
            System.out.println("obj = " + obj.getFechaNacimiento());
            System.out.println("obj = " + obj.getGenero().getDescripcion());
            Logger.getAnonymousLogger().warning("Error al crear persona");
        }
    }

    @Override
    public List<Persona> read(String criteria) {
        List<Persona> personas = new ArrayList<>();
        try {
            PostgreSQL conexion = PostgreSQL.getInstance();
            Statement st = conexion.getConn().createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s", Persona.Q_ALL, criteria));
            while(rs.next()){
                personas.add(makePersona(rs));
            }
        } catch (Exception e) {
            Logger.getAnonymousLogger().warning("Error al leer personas");
        }
        return personas;
    }

    @Override
    public Persona read(Long id) {
        Persona persona = null;
        try{
            Connection conexion = PostgreSQL.getInstance().getConn();
            Statement st = conexion.createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s", Persona.Q_BY_ID, id));
            if (rs.next()){
                persona = makePersona(rs);
            }
        }catch (ClassNotFoundException | SQLException ex){
            Logger.getAnonymousLogger().warning("Error al leer persona");
        }
        return persona;
    }

    @Override
    public void update(Persona obj) {
        try {
            PostgreSQL conexion = PostgreSQL.getInstance();
            PreparedStatement ps = conexion.getConn().prepareStatement(String.format("%s %s", Persona.UPDATE, obj.getId()));
            Integer i = 1;
            ps.setLong(i++, obj.getId());
            ps.setString(i++, obj.getNombre());
            ps.setString(i++, obj.getDomicilio());
            ps.setLong(i++, obj.getTelefono());
            ps.setString(i++, obj.getEmail());
            ps.setString(i++, obj.getFechaNacimiento());
            ps.setString(i++, obj.getGeneroDescripcion());
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getAnonymousLogger().warning("Error al actualizar persona");
        }
    }

    @Override
    public void delete(Long id) {
        try {
            PostgreSQL conexion = PostgreSQL.getInstance();
            PreparedStatement ps = conexion.getConn().prepareStatement(Persona.DELETE);
            Integer i = 1;
            ps.setLong(i++, id);
            ps.executeUpdate();
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("ex = " + ex.toString());
            Logger.getAnonymousLogger().warning("No se pudo eliminar persona");
        }
    }

    private Persona makePersona(ResultSet rs) throws SQLException {
        Persona persona = new Persona();
        Integer i = 1;

        persona.setId(rs.getLong(i++));
        persona.setNombre(rs.getString(i++));
        persona.setDomicilio(rs.getString(i++));
        persona.setTelefono(rs.getLong(i++));
        persona.setEmail(rs.getString(i++));
        persona.setFechaNacimiento(rs.getString(i++));
        //persona.setGeneroDescripcion(rs.getString(i++));
        return persona;
    }
}
