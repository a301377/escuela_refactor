package connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgreSQL {

    private Connection conn = null;
    private static PostgreSQL conexion;

    private PostgreSQL() throws SQLException, ClassNotFoundException{
        this.createConnection();
    }

    public static PostgreSQL getInstance() throws SQLException, ClassNotFoundException{
        if (conexion == null) {
            conexion = new PostgreSQL();
        }
        return conexion;
    }

    private void createConnection() throws SQLException, ClassNotFoundException{
        String urlDatabase =  "jdbc:postgresql://localhost:5432/hr2";
        Class.forName("org.postgresql.Driver");
        conn = DriverManager.getConnection(urlDatabase,  "postgres", "Azul");
    }

    public Connection getConn(){
        return conn;
    }

}
